<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->enum('jenis_kelamin', array('Laki - laki', 'Perempuan'))->nullable();
            $table->string('alamat');
            $table->string('nomor_telepon');
            $table->string('email');
            $table->string('foto_ktp');
            $table->string('doc_pendukung')->nullable();
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agencys');
    }
}

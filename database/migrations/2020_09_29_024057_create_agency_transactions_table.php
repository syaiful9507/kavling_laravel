<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencyTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_agency')->unsigned();
            $table->foreign('id_agency')->references('id')->on('agencys')->onDelete('cascade');
            $table->string('nik_buyer');
            $table->string('nama');
            $table->string('nama_kav');
            $table->string('luas');
            $table->string('block');
            $table->string('type');
            $table->bigInteger('harga_cash');
            $table->bigInteger('harga_per_meter');
            $table->bigInteger('total_harga');
            $table->bigInteger('cicilan6x');
            $table->bigInteger('total_cicilan6x');
            $table->bigInteger('cicilan12x');
            $table->bigInteger('total_cicilan12x');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_transactions');
    }
}

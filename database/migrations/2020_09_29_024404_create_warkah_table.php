<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarkahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warkah', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_transaction');
            $table->foreign('id_transaction')->references('id')->on('transactions')->onDelete('cascade');
            $table->string('block_or_number');
            $table->string('giric/c')->nullable();
            $table->string('persil')->nullable();
            $table->string('sppt')->nullable();
            $table->integer('luas');
            $table->string('utara');
            $table->string('timur');
            $table->string('selatan');
            $table->string('barat');
            $table->string('rt');
            $table->string('dusun');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warkah');
    }
}
